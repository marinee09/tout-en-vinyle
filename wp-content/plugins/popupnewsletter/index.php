<?php
/*
Plugin Name: popupnewsletter
Description: creation module newsletter pour récolte d'emails
Author: Marine
Version: 1.0.0
Author URI: https://tout-en-vinyle.fr
*/

//Variable necessaire pour ecrire/lire dans la Bdd de WP
global $wpdb;


//la fonction qui va creer la table dans la bdd à l'activation
function create_plugin_database_table()
{
    global $wpdb;

    //déclare les element comme UTF-8, ect...
    $charset_collate = $wpdb->get_charset_collate();
    //nom de la table
    $tableName = $wpdb->prefix . 'Emails';

    //requete SQL pour creer la Table
    $sql = "CREATE TABLE IF NOT EXISTS $tableName (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        email varchar(256) DEFAULT NULL,
        time datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";

    //fichier que WP a besoin si on veut mettre des maj du plugin
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    //execute la requete SQL
    dbDelta($sql);
}

//Hook d'activation du plugin qui execute la fonction : create_plugin_database_table
register_activation_hook(__FILE__, 'create_plugin_database_table');

//la fonction qui permet d'affichée le Form en front
function afficheText()
{
    echo '
    <div class="container">
        <div class="row">
            <div id="newsletter">
                 <h3>Newsletter</h3>
                 <form role="search" method="get" class="search-form" action="' . get_home_url() . '">
                     <label for="search-form-2">
                         <span class="screen-reader-text">Rechercher&nbsp;:</span>
                         <input type="text" id="search-form-2" class="search-field" placeholder="Email" value="" name="emailNews">
                     </label>
                     <input type="submit" class="search-submit" value="OK">
                 </form>
            </div>
        </div>
     </div>
     ';
}

//le Hook qui exec. la fonction  afficheText avant l'affichage du footer
add_action('get_footer', 'afficheText');


//Traitement des données Reçues

//si quelqu'un repond au formulaire il va laisser des parametres dans l'url
//alors on regarde si elles sont presentes
//si Oui : On va les traitées
if ($_GET['emailNews']) {

    //on stock l'email dans une variable pour que ce soit plus pratique
    $email = $_GET['emailNews'];

    //on regarde si on a deja l'email de la personnem (éviter les doublons)
    //on compte alors le nombre de ligne en BDD qui a le même email :

    if ($wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}emails WHERE email = '{$email}'") > 0) {
        //si le resultat est Superieure à 0
        //c'est que l'email est deja present alors on fait un return de rien.
        return;
    } else {
        //si le resultat est egal a 0

        //on stock le nom de la table
        $table = $wpdb->prefix . 'Emails';
        //on met en forme les données
        $data = array('email' => $_GET['emailNews']);
        //on ajout le format pour que la bdd les comprennent
        $format = array('%s', '%d');
        //on fait un INSERT grace à la methode de WP
        $wpdb->insert($table, $data, $format);
        //enfin on ajout l'id
        $my_id = $wpdb->insert_id;
    }
}
